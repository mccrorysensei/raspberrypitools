# README #

In order to use this you will need a Raspberry Pi already running Raspbian.

### What is this repository for? ###

* Raspberry Tools is my progress on creating a Raspberry Pi for my networking needs
* installDNSMasq sets up a DNS Caching server with ad-blocking, that updates weekly, and optionally allows for logging of DNS queries for you to monitor lookup statistics.
* setupWiFi sets up a TOR capable WiFi with a configurable SSID, but does not prevent DNS leakage, to help resolve to local sites, and enable ad-blocking.
* installBoth will enable ad-blocking and setup a TOR free WiFi node

### How do I get set up with the DNSMasq? ###

* set up your R-Pi's IP address on your router to be static (constant) and point your primary DNS on the router to the R-Pi, with the secondary DNS pointing to 8.8.8.8 (Google DNS) or another DNS of your choice *This will apparently not be possible on AT&T routers, as this setting is locked*
* ssh or log into your R-Pi
* restart to make sure you have the DNS information (this is reset at every restart of your R-Pi)
* ssh or log into your R-Pi
* Run the following two commands on the R-Pi

```
#!bash

sudo wget https://bitbucket.org/mccrorysensei/raspberrypitools/raw/master/installDNSMasq.sh
sudo bash installDNSMasq.sh
```

### How do I get set up with the WiFi? ###
* ssh or log into your R-Pi
* issue a sudo halt command
* unplug your power source after activity has stopped
* plug in your USB WiFi dongle
* plug back in the R-Pi power source
* ssh or log into your R-Pi
```
#!bash

sudo wget https://bitbucket.org/mccrorysensei/raspberrypitools/raw/master/setupWiFi.sh
sudo bash setupWiFi.sh
```

### How do I get set up with both? ###
* set up your R-Pi's IP address on your router to be static (constant) and point your primary DNS on the router to the R-Pi, with the secondary DNS pointing to 8.8.8.8 (Google DNS) or another DNS of your choice *This will apparently not be possible on AT&T routers, as this setting is locked*
* ssh or log into your R-Pi
* issue a sudo halt command
* unplug your power source after activity has stopped
* plug in your USB WiFi dongle
* plug back in the R-Pi power source
* ssh or log into your R-Pi
* Run the following two commands on the R-Pi

```
#!bash

sudo wget https://bitbucket.org/mccrorysensei/raspberrypitools/raw/master/installBoth.sh
sudo bash installBoth.sh
```

### How do I get set up the automatic R-Pi updater? ###
* ssh or log into your R-Pi
* Run the following two commands on the R-Pi
```
#!bash

sudo wget https://bitbucket.org/mccrorysensei/raspberrypitools/raw/master/autoUpdate.sh
sudo bash autoUpdate.sh
```

### Contribution guidelines ###

* Ask to pull, I will review
* Offer up new ad domains, and I will ad to the block list, everyone will get an update within the week

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact