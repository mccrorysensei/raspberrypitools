apt-get update && apt-get --yes --force-yes upgrade
apt-get --yes --force-yes install dnsutils dnsmasq
echo "Is this R-Pi dedicated to only DNS (Y/n)?"
read dedicated
# if they want to log, enable
if [ $dedicated == "Y" ] || [ $dedicated == "y" ]
then
  echo 'wget -O /etc/dnsmasq.d/adblock.conf  "https://bitbucket.org/mccrorysensei/raspberrypitools/raw/master/adblock.conf"' > /etc/cron.weekly/adblocks.sh
else
  echo 'wget -O /etc/dnsmasq.d/adblock.conf  "https://bitbucket.org/mccrorysensei/raspberrypitools/raw/master/hosts.txt"' > /etc/cron.weekly/adblocks.sh
  echo "sed -i 's/#.*//g' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
  echo "sed -i 's/\t//g' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
  echo "sed -i 's/\r//g' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
  echo "sed -i '/localhost/d' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
  echo "sed -i 's/^127.0.0.1\s/address=\//g' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
  echo "sed -i '/address/!d' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
  echo "sed -i 's/$/\/0.0.0.0/' /etc/dnsmasq.d/adblock.conf" >> /etc/cron.weekly/adblocks.sh
fi

echo "service dnsmasq restart" >> /etc/cron.weekly/adblocks.sh
chmod +x /etc/cron.weekly/adblocks.sh
bash /etc/cron.weekly/adblocks.sh
# increase maximum cache size
echo "cache-size=2048" >> /etc/dnsmasq.conf
echo "Do you want to log DNS (Y/n)?"
read logger
# if they want to log, enable
if [ $logger == "Y" ] || [ $logger == "y" ]
then
  echo "log-queries" >> /etc/dnsmasq.conf
fi