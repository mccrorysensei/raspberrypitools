echo "We are updating the system, this is a good opportunity to go to http://freedns.afraid.org/ and get your public static IP set up if you don't already have one!"
apt-get update && apt-get --yes --force-yes upgrade

echo "Now, time to install the VPN software"
apt-get --yes --force-yes install openvpn

echo "Let's create our keys"
mkdir /etc/openvpn/easy-rsa
cp -r /usr/share/doc/openvpn/examples/easy-rsa/2.0/ /etc/openvpn/easy-rsa/
sed -i -e 's/export EASY_RSA="`pwd`"/export EASY_RSA="\/etc\/openvpn\/easy-rsa/2.0"/g' /etc/openvpn/easy-rsa/2.0/vars
sed -i -e 's/export KEY_SIZE=1024/export KEY_SIZE=2048/g' /etc/openvpn/easy-rsa/2.0/vars

echo "Optional all the way, but you can fill in Common Name instead"
source /etc/openvpn/easy-rsa/2.0/vars
bash /etc/openvpn/easy-rsa/2.0/clean-all
bash /etc/openvpn/easy-rsa/2.0/build-ca

echo "What did you name your server?"
read serverName

echo "Common Name must be the same, challenge password must be blank, and answer y for the sign, and for the commit. Everything else is optional."
bash /etc/openvpn/easy-rsa/2.0/build-key-server $serverName

echo "What user name do you want?"
read userName

echo "You must be able to remember the PEM passphrase, challenge password must be blank, and answer y for the sign, and for the commit. Everything else is optional."
bash /etc/openvpn/easy-rsa/2.0/build-key-pass $userName

echo "You will be prompted for your previously entered passphrase 3 times to encrypt the key"
openssl rsa -in /etc/openvpn/easy-rsa/2.0/keys/$userName.key -des3 out /etc/openvpn/easy-rsa/2.0/keys/$userName.3des.key

echo "This will take a few minutes, go grab a drink, and wash your face"
bash /etc/openvpn/easy-rsa/2.0/build-dh

echo "Enabling the DoS attack protection"
openvpn --genkey --secret /etc/openvpn/easy-rsa/2.0/keys/ta.key

echo "Configuring the VPN"
echo "What is your R-Pi's IP address?"
read ipAddress
echo "What is your router's IP Address?"
read routerIPAdress
echo "local $ipAddress # SWAP THIS NUMBER WITH YOUR RASPBERRY PI IP ADDRESS" > /etc/openvpn/server.conf
echo "dev tun" >> /etc/openvpn/server.conf
echo "proto udp #Some people prefer to use tcp. Don't change it if you don't know." >> /etc/openvpn/server.conf
echo "port 1194" >> /etc/openvpn/server.conf
echo "ca /etc/openvpn/easy-rsa/2.0/keys/ca.crt" >> /etc/openvpn/server.conf
echo "cert /etc/openvpn/easy-rsa/2.0/keys/$serverName.crt # SWAP WITH YOUR CRT NAME" >> /etc/openvpn/server.conf
echo "key /etc/openvpn/easy-rsa/2.0/keys/$serverName.key # SWAP WITH YOUR KEY NAME" >> /etc/openvpn/server.conf
echo "dh /etc/openvpn/easy-rsa/2.0/keys/dh2048.pem # If you changed to 2048, change that here!" >> /etc/openvpn/server.conf
echo "server 10.8.0.0 255.255.255.0" >> /etc/openvpn/server.conf
echo "# server and remote endpoints" >> /etc/openvpn/server.conf
echo "ifconfig 10.8.0.1 10.8.0.2" >> /etc/openvpn/server.conf
echo "# Add route to Client routing table for the OpenVPN Server" >> /etc/openvpn/server.conf
echo 'push "route 10.8.0.1 255.255.255.255"' >> /etc/openvpn/server.conf
echo "# Add route to Client routing table for the OpenVPN Subnet" >> /etc/openvpn/server.conf
echo 'push "route 10.8.0.0 255.255.255.0"' >> /etc/openvpn/server.conf
echo "# your local subnet" >> /etc/openvpn/server.conf
echo 'push "route $ipAddress 255.255.255.0" # SWAP THE IP NUMBER WITH YOUR RASPBERRY PI IP ADDRESS' >> /etc/openvpn/server.conf
echo "# Set primary domain name server address to the SOHO Router" >> /etc/openvpn/server.conf
echo "# If your router does not do DNS, you can use Google DNS 8.8.8.8" >> /etc/openvpn/server.conf
echo 'push "dhcp-option DNS $routerIPAdress" # This should already match your router address and not need to be changed.' >> /etc/openvpn/server.conf
echo "# Override the Client default gateway by using 0.0.0.0/1 and" >> /etc/openvpn/server.conf
echo "# 128.0.0.0/1 rather than 0.0.0.0/0. This has the benefit of" >> /etc/openvpn/server.conf
echo "# overriding but not wiping out the original default gateway." >> /etc/openvpn/server.conf
echo 'push "redirect-gateway def1"' >> /etc/openvpn/server.conf
echo "client-to-client" >> /etc/openvpn/server.conf
echo "duplicate-cn" >> /etc/openvpn/server.conf
echo "keepalive 10 120" >> /etc/openvpn/server.conf
echo "tls-auth /etc/openvpn/easy-rsa/2.0/keys/ta.key 0" >> /etc/openvpn/server.conf
echo "cipher AES-128-CBC" >> /etc/openvpn/server.conf
echo "comp-lzo" >> /etc/openvpn/server.conf
echo "user nobody" >> /etc/openvpn/server.conf
echo "group nogroup" >> /etc/openvpn/server.conf
echo "persist-key" >> /etc/openvpn/server.conf
echo "persist-tun" >> /etc/openvpn/server.conf
echo "status /var/log/openvpn-status.log 20" >> /etc/openvpn/server.conf
echo "log /var/log/openvpn.log" >> /etc/openvpn/server.conf
echo "verb 1" >> /etc/openvpn/server.conf

echo "enabling packet forwarding"
sed -i -e 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sysctl -p
echo "#!/bin/sh
### BEGIN INIT INFO" > /etc/init.d/vpnFirewallRule
echo "# Provides:          vpnFirewallRule" >> /etc/init.d/vpnFirewallRule
echo "# Required-Start:    $local_fs $network" >> /etc/init.d/vpnFirewallRule
echo "# Required-Stop:     $local_fs" >> /etc/init.d/vpnFirewallRule
echo "# Default-Start:     2 3 4 5" >> /etc/init.d/vpnFirewallRule
echo "# Default-Stop:      0 1 6" >> /etc/init.d/vpnFirewallRule
echo "# Short-Description: vpnFirewallRule" >> /etc/init.d/vpnFirewallRule
echo "# Description:       VPN special Firewall Rule" >> /etc/init.d/vpnFirewallRule
echo "### END INIT INFO" >> /etc/init.d/vpnFirewallRule
echo "iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j SNAT --to-source $ipAddress" >> /etc/init.d/vpnFirewallRule
# update the permissions
chmod 755 /etc/init.d/vpnFirewallRule
update-rc.d vpnFirewallRule defaults

echo "Getting ready for your client setup"
echo "client 
dev tun 
proto udp" > /etc/openvpn/easy-rsa/2.0/keys/default.txt
echo "Do you have a public IP?"
read publicIPSwitch
if [ $publicIPSwitch == "Y" ] || [ $publicIPSwitch == "y" ]
then
	echo "What is your public IP?"
	read publicIP
	echo "remote $publicIP 1194" >> /etc/openvpn/easy-rsa/2.0/keys/default.txt
else
	echo "What is your public domain from http://freedns.afraid.org/?"
	read publicIP
	echo "What is your login for Free DNS?"
	read dnsLogin
	echo "What is your password for Free DNS?"
	read dnsPass
	#setup ddclient for dynamic IP
	apt-get --yes --force-yes install ddclient
	echo "daemon=600                              # check every 300 seconds
	syslog=yes                              # log update msgs to syslog
	mail=root                               # mail all msgs to root
	mail-failure=root                       # mail failed update msgs to root
	pid=/var/run/ddclient.pid               # record PID in file.
	ssl=yes                                 # use ssl-support.  Works with
	# ssl-library
	use=if, if=eth0        # get ip from server.
	server=freedns.afraid.org               # default server
	login=$dnsLogin                # default login
	password=$dnsPass              # default password
	protocol=dyndns1                      
	$publicIP" > /etc/ddclient.conf 
	ddclient
	echo "remote $publicIP 1194" >> /etc/openvpn/easy-rsa/2.0/keys/default.txt
	# setup ddclient for run on boot
	echo "#!/bin/sh
	### BEGIN INIT INFO" > /etc/init.d/ddclientStartup
	echo "# Provides:          ddclientStartup" >> /etc/init.d/ddclientStartup
	echo "# Required-Start:    $local_fs $network" >> /etc/init.d/ddclientStartup
	echo "# Required-Stop:     $local_fs" >> /etc/init.d/ddclientStartup
	echo "# Default-Start:     2 3 4 5" >> /etc/init.d/ddclientStartup
	echo "# Default-Stop:      0 1 6" >> /etc/init.d/ddclientStartup
	echo "# Short-Description: ddclientStartup" >> /etc/init.d/ddclientStartup
	echo "# Description:       DD Client launch on startup rule" >> /etc/init.d/ddclientStartup
	echo "### END INIT INFO" >> /etc/init.d/ddclientStartup
	echo "ddclient" >> /etc/init.d/ddclientStartup
	# update the permissions
	chmod 755 /etc/init.d/ddclientStartup
	update-rc.d ddclientStartup defaults
fi

echo"resolv-retry infinite 
nobind 
persist-key 
persist-tun 
mute-replay-warnings 
ns-cert-type server 
key-direction 1 
cipher AES-128-CBC 
comp-lzo 
verb 1 
mute 20" >> /etc/openvpn/easy-rsa/2.0/keys/default.txt

echo "#!/bin/bash
# Default Variable Declarations" > /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'DEFAULT="default.txt"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'FILEEXT=".ovpn"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'CRT=".crt"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'KEY=".3des.key"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'CA="ca.crt"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'TA="ta.key"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Ask for a Client name' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "Please enter an existing Client Name:"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'read NAME' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#1st Verify that client�s Public Key Exists' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'if [ ! -f $NAME$CRT ]; then' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' echo "[ERROR]: Client Public Key Certificate not found: $NAME$CRT"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' exit' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'fi' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "Client�s cert found: $NAME$CR"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Then, verify that there is a private key for that client' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'if [ ! -f $NAME$KEY ]; then' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' echo "[ERROR]: Client 3des Private Key not found: $NAME$KEY"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' exit' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'fi' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "Client�s Private Key found: $NAME$KEY"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Confirm the CA public key exists' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'if [ ! -f $CA ]; then' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' echo "[ERROR]: CA Public Key not found: $CA"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' exit' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'fi' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "CA public Key found: $CA"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Confirm the tls-auth ta key file exists' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'if [ ! -f $TA ]; then' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' echo "[ERROR]: tls-auth Key not found: $TA"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo ' exit' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'fi' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "tls-auth Private Key found: $TA"' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Ready to make a new .opvn file - Start by populating with the default file' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'cat $DEFAULT > $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Now, append the CA Public Cert' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "<ca>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'cat $CA >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "</ca>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh

echo '#Next append the client Public Cert' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "<cert>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo "cat $NAME$CRT | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> $NAME$FILEEXT" >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "</cert>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Then, append the client Private Key' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "<key>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'cat $NAME$KEY >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "</key>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo '#Finally, append the TA Private Key' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "<tls-auth>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'cat $TA >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
echo 'echo "</tls-auth>" >> $NAME$FILEEXT' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
 
echo 'echo "Done! $NAME$FILEEXT Successfully Created."' >> /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh

chmod 755 /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh
bash /etc/openvpn/easy-rsa/2.0/keys/MakeOVPN.sh

cp /etc/openvpn/easy-rsa/2.0/keys/$userName.ovpn /home/pi

echo "You should now use SCP to get the file you just created"
echo "From a linux computer, you can run:  scp pi@$ipAddress:/home/pi/$userName.ovpn /tmp"
echo "From a non-Linux computer you'll need an SCP client, such as WinSCP or Fugu"

echo "You can then, use a VPN client such as OpenVPN connect"