 # Let user know what is happening
echo "This script will now configure your Raspberry Pi as an unsecured wireless access point."
# Make sure up to date
apt-get update && apt-get --yes --force-yes upgrade
# Install drivers for WiFi
apt-get --yes --force-yes install hostapd dnsmasq
# serve up DHCP from DNSMasq to wlan0
echo "domain=tor.com
dhcp-range=wlan0,10.42.42.100,10.42.42.155,4h" > /etc/dnsmasq.d/freeTor.conf

#configure wlan0 to have the static IP in the interfaces file
sed -i -e 's/iface wlan0 inet manual/iface wlan0 inet static\n         address 10.42.42.1\n        netmask 255.255.255.0/g' /etc/network/interfaces
echo "up iptables-restore < /etc/iptables.ipv4.nat" >> /etc/network/interfaces
#save
iptables-save > /etc/iptables.ipv4.nat
#configure actual static IP
ifconfig wlan0 10.42.42.1

#configuring hostapd

#get SSID
echo "Press Enter for the default SSID, or type your 1-32 character SSID"
read ssid
#check for correct length
until [ ${#ssid} -lt 32 ]; do
  echo "Your entered SSID was too long. Please try again."
  echo "Press Enter for the default SSID, or type your 1-32 character SSID"
  read ssid
done
#check for default of special
if [ ${#ssid} -eq 0 ]
then
  ssid="attwifi"
fi
#set options for the access point

#in ubuntu you have to change the driver to nl80211
# and run the following commands
#  nmcli nm wifi off
#  rfkill unblock wlan
#  ifconfig wlan1 10.42.42.1 up
# depending on adapter you may need to swap out with another driver, rtl871xdrv and nl80211 are the most common

echo "interface=wlan0
driver=rtl871xdrv
ssid=$ssid
hw_mode=g
channel=6
ignore_broadcast_ssid=0
country_code=US
ht_capab=[HT40-][SHORT-GI-40][TX-STBC][RX-STBC12]
eap_reauth_period=86400
auth_algs=1" > /etc/hostapd/tor.conf
#set to run at startup
sed -i -e 's/#DAEMON_CONF=""/DAEMON_CONF="\/etc\/hostapd\/tor.conf"/g' /etc/default/hostapd
update-rc.d hostapd enable




#setting up IP tables
# iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
# iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
# iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
# sh -c "iptables-save > /etc/iptables.ipv4.nat"
service hostapd stop
service hostapd start



#as this is your open WiFi, time to setup TOR
echo "Setting up TOR routing to keep those free WiFi lovers from your stuff"
apt-get --yes --force-yes install tor
#stop to be able to edit config
service tor stop
#make a copy of the setting file
mv /etc/tor/torrc /etc/tor/torrc.bak
#we have removed the DNS listening property
echo "Log notice file /var/log/tor/notices.log
VirtualAddrNetwork 10.192.0.0/10
AutomapHostsSuffixes .onion,.exit
AutomapHostsOnResolve 1
TransPort 9040
TransListenAddress 10.42.42.1" > /etc/tor/torrc

# #flush IP tables
# iptables -F
# iptables -t nat -F
# #set ssh exemption, very important to use your pi
# iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 22 -j REDIRECT --to-ports 22



# #set up logging
touch /var/log/tor/notices.log
chown debian-tor /var/log/tor/notices.log
chmod 644 /var/log/tor/notices.log
#start TOR
service tor start
#set TOR to start at boot
update-rc.d tor enable

#set IP forwarding to run at startup
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
#activate IP forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
#rerouting TCP traffice
iptables -t nat -A PREROUTING -i wlan0 -p tcp --syn -j REDIRECT --to-ports 9040
#save IP tables
iptables-save > /etc/iptables.ipv4.nat

#DNS from DNSMasq

# restart the DNS server so it serves DHCP too
service dnsmasq restart

# Some R-Pis don't force the configured IP address
# create file for running at start up
echo "#!/bin/sh
### BEGIN INIT INFO
# Provides:          torspecial
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: torspecial
# Description:       torspecial instructions for the wlan0
### END INIT INFO
ifconfig wlan0 10.42.42.1
service tor start
iptables -t nat -A PREROUTING -i wlan0 -p tcp --syn -j REDIRECT --to-ports 9040" > /etc/init.d/torspecial
# update the permissions
chmod 755 /etc/init.d/torspecial
update-rc.d torspecial defaults